package model.compiler

import model.codeRepresentation.aClass
import model.utils.a
import org.scalatest.FunSuite

class CompilerLogicSpec extends FunSuite {

  test("Compiler Model") {
    info("a compiler can be created")
    val compiler = a(Compiler)

    info("a compiler can receive code")
    val code1 = "a(Compiler)"
    compiler.receive(code1)

    info("a compiler can compile the code")
    compiler.compile

    info("compiled result is a correct code representation")
    assert(compiler.compiledResult.isInstanceOf[List[aClass]])

    info("a compiler can receive code after being compiled")
    compiler.receive(code1)

    info("compiled result cannot be obtained if code has not being compiled")
    compiler.receive(code1)
    assertThrows[Compiler.NotCompiled](compiler.compiledResult)
  }
}
