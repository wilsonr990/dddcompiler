package model.codeRepresentation

import org.scalatest.FunSuite
import model.utils.{a, cantBuild}

class ParameterSpec extends FunSuite {
  test("Code representation") {
    info("a parameter can be created with a name")
    a(Parameter.withName("param1"))

    info("a parameter cannot be created without a Name")
    assert(cantBuild(Parameter))
  }
}
