package model.codeRepresentation

import model.utils.a
import org.scalatest.FunSuite

class AClassSpec extends FunSuite {
  test("Code representation") {
    info("a class can be created with a name and no parameters")
    val class1 = a(aClass.withName("aClass"))

    info("a class can be created with parameters")
    val class2 = a(aClass.withName("aClass").withParameter(a(Parameter.withName("Name"))))
    val class3 = a(
      aClass
        .withName("aClass")
        .withParameter(a(Parameter.withName("Name")))
        .withParameter(a(Parameter.withName("Parameter"))))

    info("a class cannot be created without a Name")
    assertThrows[aClass.NoNamePropProvided](a(aClass))

    info("a class without parameters can be printed")
    assert(
      class1.ttoString ==
        s"""package model.codeRepresentation
           |
           |import model.utils._
           |
           |sealed case class aClass()
           |
           |object aClass extends Builder[aClass] {
           |  override def build(): aClass = {
           |    if (name.isEmpty) throw NoNamePropProvided()
           |    new aClass(name.get)
           |  }
           |}""".stripMargin)

    info("a class with parameters can be printed")
    assert(
      class3.ttoString ==
        s"""package model.codeRepresentation
           |
           |import model.utils._
           |
           |sealed case class aClass(parameter: String, name: String)
           |
           |object aClass extends Builder[aClass] {
           |  var parameter: Option[String] = None
           |  var name: Option[String] = None
           |
           |  def withParameter(parameter: String): aClass.type = {
           |    this.parameter = Some(parameter)
           |    this
           |  }
           |
           |  def withName(name: String): aClass.type = {
           |    this.name = Some(name)
           |    this
           |  }
           |
           |  override def build(): aClass = {
           |    if (name.isEmpty) throw NoNamePropProvided()
           |    new aClass(name.get)
           |  }
           |
           |  case class NoParameterPropProvided() extends CustomException(this, RawStatus((parameter)), "building")
           |  case class NoNamePropProvided() extends CustomException(this, RawStatus((name)), "building")
           |}""".stripMargin)
  }
}
