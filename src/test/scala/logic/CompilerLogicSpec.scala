package logic

import model.codeRepresentation.{Parameter, aClass}
import model.utils.a
import org.scalatest.FunSuite

class CompilerLogicSpec extends FunSuite {
  test("compiling a new class") {
    assert(CompilerLogic.compileFunction("a(NewClass)") contains a(aClass.withName("NewClass")))
    assert(CompilerLogic.compileFunction("a(\n    NewClass\n)") contains a(aClass.withName("NewClass")))
  }

  test("compiling two classes") {
    val compilation = CompilerLogic.compileFunction("a(NewClass)\na(AnotherClass)")
    assert(compilation contains a(aClass.withName("NewClass")))
    assert(compilation contains a(aClass.withName("AnotherClass")))
  }

  test("compiling with variable creation") {
    val compilation = CompilerLogic.compileFunction(
      "val class1 = a(NewClass)\n" +
        "val class2 = a(AnotherClass)")
    assert(compilation contains a(aClass.withName("NewClass")))
    assert(compilation contains a(aClass.withName("AnotherClass")))
  }

  test("a class with one string parameter") {
    val compilation = CompilerLogic.compileFunction("val class1 = a(NewClass.withParameter(\"value\"))")
    assert(compilation contains a(aClass.withName("NewClass").withParameter(a(Parameter.withName("Parameter")))))
    val compilation2 = CompilerLogic.compileFunction("val class1 = a(\n  NewClass\n  .withParameter(\"value\")\n  .withParameter2(\"value2\"))")
    assert(
      compilation2 contains a(
        aClass.withName("NewClass").withParameter(a(Parameter.withName("Parameter"))).withParameter(a(Parameter.withName("Parameter2")))))
  }

  test("a class with two string parameters") {
    val compilation = CompilerLogic.compileFunction(
      "val class1 = a(\n  NewClass\n  .withParameter(\"value\")\n  " +
        ".withParameter2(\"value2\"))")
    assert(
      compilation contains a(
        aClass.withName("NewClass").withParameter(a(Parameter.withName("Parameter"))).withParameter(a(Parameter.withName("Parameter2")))))
  }

  test("a class with two description usage") {
    val compilation = CompilerLogic.compileFunction(
      "val class1 = a(\n  NewClass\n  .withParameter(\"value\"))\n  " +
        "val class2 = a(\n  NewClass\n  .withParameter(\"value\"))\n ")
    assert(compilation.length === 1)
    assert(compilation contains a(aClass.withName("NewClass").withParameter(a(Parameter.withName("Parameter")))))
  }
}
