import java.io.{BufferedWriter, File, FileWriter}
import java.nio.file.{Files, Paths}

import model.compiler.Compiler
import model.utils.a

object Main extends App {
  // TODO: Calculate the path of the files to compile (they're inside of model folder) instead of putting them here
  val fileToCompile = "/Users/w.arevalo/Documents/RGA/dddcompiler/src/test/scala/model/codeRepresentation/ParameterSpec.scala"
  val fileToResult  = "/Users/w.arevalo/Documents/RGA/dddcompiler/src/main/scala/model/codeRepresentation/Parameter.scala"

  val content = new String(Files.readAllBytes(Paths.get(fileToCompile)))
  val result  = a(Compiler).receive(content).compile

  result.compiledResult
    .map(_.ttoString)
    .foreach(content => {
      val file = new File(fileToResult)
      val bw   = new BufferedWriter(new FileWriter(file))
      bw.write(content)
      bw.close()
    })

}
