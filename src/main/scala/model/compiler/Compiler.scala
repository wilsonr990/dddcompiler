package model.compiler
import logic.CompilerLogic
import model.codeRepresentation.aClass
import model.compiler.Compiler.{Compiled, WaitingCode, WithCode}
import model.utils._

sealed case class Compiler(id: ID) {
  var status: Status = Compiler.WaitingCode()

  def receive(code: String): Compiler = {
    status match {
      case WaitingCode() =>
        status = Compiler.WithCode(code)
      case WithCode(_) =>
        status = Compiler.WithCode(code)
      case Compiled(_) =>
        status = Compiler.WithCode(code)
      case _ =>
        throw Compiler.NotReadyToReceive(this, status, code)
    }
    this
  }

  def compile: Compiler = {
    status match {
      case Compiler.WithCode(code) =>
        status = Compiler.Compiled(CompilerLogic.compileFunction(code))
      case _ =>
        throw Compiler.NotReadyToCompile(this, status)
    }
    this
  }

  def compiledResult: List[aClass] = {
    status match {
      case Compiler.Compiled(result) =>
        result
      case _ =>
        throw Compiler.NotCompiled(this, status)
    }
  }
}

object Compiler extends Builder[Compiler] {
  case class WaitingCode()                  extends Status()
  case class WithCode(code: String)         extends Status()
  case class Compiled(result: List[aClass]) extends Status()

  override def build(): Compiler = {
    new Compiler(uniqueId())
  }

  case class NotReadyToCompile(sender: Compiler, status: Status) extends CustomException(sender, status, null)

  case class NotCompiled(sender: Compiler, status: Status) extends CustomException(sender, status, null)

  case class NotReadyToReceive(sender: Compiler, status: Status, code: String) extends CustomException(sender, status, code)
}
