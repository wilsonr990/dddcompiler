package model.utils
import java.util.UUID

class Status()
case class RawStatus(a: Any) extends Status()

class ID(id: Any)
case class uniqueId(uuid: UUID = UUID.randomUUID()) extends ID(uuid)
