package model.utils

class CustomException(sender: Any, status: Status, intention: Any) extends Exception() {
  override def getMessage: String = {
    def beautify(obj: Any) =
      obj.getClass.getName
        .replaceFirst(".*\\.", "")
        .replaceAll("([A-Z])", " $1")
        .replaceAll("([$])", "")
        .trim
    s"${beautify(this)}:\n  Sender -> $sender\n  Status -> $status\n  Intention -> $intention"
  }
}
