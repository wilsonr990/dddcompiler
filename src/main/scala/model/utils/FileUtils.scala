package model.utils

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.file.{Files, NoSuchFileException, Paths}

object FileUtils {

  def getFileTree(f: File): Stream[File] =
    f #:: (if (f.isDirectory) f.listFiles().toStream.flatMap(getFileTree)
           else Stream.empty)

  def getFilesWithExtensionInPath(path: String, ext: String): List[String] = {
    val folder = new File(path)
    getFileTree(folder).collect {
      case file if file.getName.endsWith(ext) => file.getPath
    }.toList
  }

  def getFilesWithExtensionInResourcesPath(path: String, ext: String): List[String] = {
    val folder = new File(parseFilePathFromResources(path))
    getFileTree(folder).collect {
      case file if file.getName.endsWith(ext) => file.getPath
    }.toList
  }

  def simplifyPath(path: String): String =
    path match {
      case s if s contains "/.." =>
        simplifyPath(path.replaceAll("/[^/]*/\\.\\.", ""))
      case _ =>
        path
    }

  def parseFilePathFromResources(path: String): String = {
    val fullPath = simplifyPath(getClass.getResource("/").getPath + path)
    if (fullPath.startsWith("/C:"))
      fullPath.substring(1).replace("/", "\\").replace("%20", " ")
    else
      fullPath.replace("%20", " ")
  }

  def readFileFromResources(path: String): String = {
    val fullPath = parseFilePathFromResources(path)
    try {
      new String(Files.readAllBytes(Paths.get(fullPath)))
    } catch {
      case e: NoSuchFileException =>
        ""
    }
  }

  def saveStringToResources(input: String, path: String) {
    val fullPath = parseFilePathFromResources(path)

    val file = new File(fullPath)
    val bw   = new BufferedWriter(new FileWriter(file))
    bw.write(input)
    bw.close()
  }
}
