package model.utils

import java.lang.reflect.Field

import scala.util.Try

trait Builder[T] {
  def buildAndReset(): T = {
    val obj = build()
    reset()
    obj
  }
  def build(): T
  def reset() {
    val fields: Array[Field] = this.getClass.getDeclaredFields
    fields.foreach(f => {
      f.setAccessible(true)
      f.get(this) match {
        case _: Option[Any]   => f.set(this, None)
        case _: List[Any]     => f.set(this, List.empty)
        case _: Set[Any]      => f.set(this, Set.empty)
        case _: Map[Any, Any] => f.set(this, Map.empty)
        case _: Integer       => f.set(this, 0)
        case _                =>
      }
    })
  }
}

object a {
  def apply[T](obj: Builder[T]): T = obj.buildAndReset()
}

object cantBuild {
  def apply[T](f: Builder[T]): Boolean = Try { a(f) }.isFailure
}
