package model.codeRepresentation

import model.utils._

sealed case class Parameter(name: String)

object Parameter extends Builder[Parameter] {
  var name: Option[String] = None

  def withName(name: String): Parameter.type = {
    this.name = Some(name)
    this
  }

  override def build(): Parameter = {
    if (name.isEmpty) throw NoNamePropProvided()
    new Parameter(name.get)
  }

  case class NoNamePropProvided() extends CustomException(this, RawStatus((name)), "building")
}
