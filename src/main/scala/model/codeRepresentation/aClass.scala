package model.codeRepresentation

import model.utils._

sealed case class aClass(name: String, parameters: List[Parameter]) {
  var status: Status = aClass.Initial()

  def ttoString: String = status match {
    case aClass.Initial() =>
      "package model.codeRepresentation\n\n" +
        "import model.utils._\n\n" +
        parameters
          .map(param => s"${param.name.toLowerCase}: String")
          .mkString(s"sealed case class $name(", ", ", ")\n\n") +
        s"object $name extends Builder[$name] {\n" +
        parameters
          .map(param => s"  var ${param.name.toLowerCase}: Option[String] = None")
          .mkString("", "\n", if (parameters.nonEmpty) "\n\n" else "") +
        parameters
          .map(
            param =>
              s"  def with${param.name}(${param.name.toLowerCase}: String): $name.type = {\n" +
                s"    this.${param.name.toLowerCase} = Some(${param.name.toLowerCase})\n" +
                s"    this\n" +
              "  }")
          .mkString("", "\n\n", if (parameters.nonEmpty) "\n\n" else "") +
        s"  override def build(): $name = {\n" +
        s"    if (name.isEmpty) throw NoNamePropProvided()\n" +
        s"    new $name(name.get)\n" +
        s"  }\n" +
        parameters
          .map(param =>
            s"  case class No${param.name}PropProvided() extends CustomException(this, RawStatus((${param.name.toLowerCase})), " +
            "\"building\")")
          .mkString(if (parameters.nonEmpty) "\n" else "", "\n", if (parameters.nonEmpty) "\n" else "") +
        "}"
    case _ => ""
  }
}
object aClass extends Builder[aClass] {
  private final case class Initial() extends Status()

  var name: Option[String]        = None
  var parameters: List[Parameter] = List.empty

  def withName(name: String): aClass.type = {
    this.name = Some(name)
    this
  }

  def withParameter(parameter: Parameter): aClass.type = {
    this.parameters ::= parameter
    this
  }

  override def build(): aClass = {
    if (name.isEmpty) throw NoNamePropProvided()
    new aClass(name.get, parameters)
  }

  case class NoNamePropProvided()                                       extends CustomException(this, RawStatus((name)), "building")
  case class NotWaiting(sender: aClass, status: Status, intention: Any) extends CustomException(sender, status, intention)
}
