package logic
import model.codeRepresentation.{Parameter, aClass}
import model.utils.a

object CompilerLogic {
  def compileFunction(code: String): List[aClass] = {
    val aClassRegex = "a\\(\\s*([a-zA-Z1-9]+)((?:\\s*\\.with[a-zA-Z1-9]+\\(\\\"[a-zA-Z1-9]+\\\"\\))*)\\s*\\)".r
    val paramsRegex = "with([a-zA-Z1-9]+)\\(\\\"([a-zA-Z1-9]+)\\\"\\)".r

    aClassRegex
      .findAllIn(code)
      .toSet[String]
      .map {
        case aClassRegex(className, params) =>
          val classToBuild = aClass.withName(className)
          paramsRegex.findAllIn(params).foreach {
            case paramsRegex(name, _) =>
              classToBuild.withParameter(a(Parameter.withName(name)))
          }
          a(classToBuild)
      }
      .toList
  }
}
